import { useState } from "react";

import ContactCard from "./components/ContactCard/ContactCard";
import ContactList from "./components/ContactsList/ContactList";
import './App.css';

import { Button } from "reactstrap";
import { v4 as uuid4 } from 'uuid';
import RegisterModal from "./components/RegisterForm/RegisterForm";

const items = [
  {
    id: uuid4(),
    name: 'Denis Kobzar',
    age: 21,
    phone: '+38071732980',
    link: 'https://www.linkedin.com/in/denis-kobzar-570b84205/'
  },
  {
    id: uuid4(),
    name: 'Oleg Gromov',
    age: 22,
    phone: '+380098674210',
    link: 'https://www.linkedin.com/in/denis-kobzar-570b84205/'
  },
  {
    id: uuid4(),
    name: 'Igor Kobla',
    age: 23,
    phone: '+380912345913',
    link: 'https://www.linkedin.com/in/denis-kobzar-570b84205/'
  },
  {
    id: uuid4(),
    name: 'Olga Flower',
    age: 27,
    phone: '+380050123456',
    link: 'https://www.linkedin.com/in/denis-kobzar-570b84205/'
  }
];

function App() {

  const [contacts, setContacts] = useState(items);
  const [modalVisible, setModalVisible] = useState(false);
  const [activeContact, setActiveContact] = useState(null);

  const addContact = (contact) => {
    contact.id = uuid4();
    const newContacts = [...contacts, contact];
    setContacts(newContacts);
    setModalVisible(false);
  };

  const itemOnClickHandler = (id) => {
    setActiveContact(contacts.find((item) => item.id === id));
  };

  const removeItemHandler = (id) => {
    const newContacts = contacts.filter((item) => item.id !== id);
    setContacts(newContacts);
    setActiveContact(null);
  }

  return (
    <div className="App">
      <div className="container pt-3">
        <h1 className="pb-2">Contacts Manager App</h1>

        <div className="row">
          <div className="col">
            <ContactList
              contacts={contacts}
              setActiveContact={setActiveContact}
              onClick={itemOnClickHandler} />
          </div>
          <div className="col">
            <ContactCard
              contact={activeContact}
              onRemove={removeItemHandler} />
          </div>
        </div>

        <Button
          className="add-button mt-3"
          color="primary"
          onClick={() => setModalVisible(true)}>
          Add contact
        </Button>

        <RegisterModal
          visible={modalVisible}
          onClose={setModalVisible.bind(this, false)}
          onSubmit={addContact} />
      </div>
    </div>
  );
}

export default App;
