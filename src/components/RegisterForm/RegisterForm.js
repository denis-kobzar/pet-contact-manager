import { Component } from "react";

import "./RegisterForm.css";
import {Button, Form, FormGroup, Input, Label, Modal, ModalBody, ModalFooter, ModalHeader} from "reactstrap";

export default class RegisterForm extends Component {

  state = {
    id: '',
    name: '',
    age: '',
    phone: '',
    link: ''
  }

  inputNameHandler = (event) => {
    this.setState({
      name: event.target.value
    });
  }

  inputAgeHandler = (event) => {
    this.setState({
      age: event.target.value
    });
  }

  inputPhoneHandler = (event) => {
    this.setState({
      phone: event.target.value
    });
  }

  inputLinkHandler = (event) => {
    this.setState({
      link: event.target.value
    });
  }

  submit = () => {
    this.props.onSubmit(this.state);
    this.props.onClose();
  }

  render() {
    return (
      <div className="register-form">
        <Modal isOpen={this.props.visible} toggle={this.props.onClose}>
          <ModalHeader>Add Contact Modal</ModalHeader>
          <ModalBody>
            <Form inline>

              <FormGroup floating>
                <Input
                  id="contactName"
                  name="name"
                  placeholder="Name"
                  type="text"
                  onChange={this.inputNameHandler}
                />
                <Label for="contactName">
                  Name
                </Label>
              </FormGroup>

              <FormGroup floating>
                <Input
                  id="contactAge"
                  name="age"
                  placeholder="Age"
                  type="text"
                  onChange={this.inputAgeHandler}
                />
                <Label for="contactAge">
                  Age
                </Label>
              </FormGroup>

              <FormGroup floating>
                <Input
                  id="contactPhone"
                  name="phone"
                  placeholder="Phone"
                  type="text"
                  onChange={this.inputPhoneHandler}
                />
                <Label for="contactPhone">
                  Phone
                </Label>
              </FormGroup>

              <FormGroup floating>
                <Input
                  id="contactLink"
                  name="link"
                  placeholder="LinkedIn Link"
                  type="text"
                  onChange={this.inputLinkHandler}
                />
                <Label for="contactLink">
                  LinkedIn Link
                </Label>
              </FormGroup>

            </Form>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.submit}>Add contact</Button>{' '}
            <Button color="secondary" onClick={this.props.onClose}>Cancel</Button>
          </ModalFooter>
        </Modal>
      </div>
    )
  }
}
