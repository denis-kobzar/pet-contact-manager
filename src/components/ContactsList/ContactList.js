import { Component } from "react";

import "./ContactList.css";

import {Card, ListGroup, ListGroupItem} from "reactstrap";

export default class ContactList extends Component {

  render() {
    return (
      <Card className="contact-list">
        <ListGroup>
          {
            this.props.contacts.map((contact, index) => {
              return (
                <ListGroupItem
                  key={index}
                  className="contact-list__item"
                  action
                  tag="div"
                  onClick={this.props.onClick.bind(this, contact.id)}
                >
                  { contact.name }
                </ListGroupItem>
              )
            })
          }
        </ListGroup>
      </Card>
    )
  }
}
