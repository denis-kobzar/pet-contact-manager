import { Component } from "react";

import "./ContactCard.css";
import {Card, CardText, CardTitle, Button} from "reactstrap";

export default class ContactCard extends Component {

  render() {
    if (this.props.contact != null)
      return (
        <div className="contact-card">
          <Card
            body
            className="contact-card__body">
            <div className="contact-card__top">
              <CardTitle tag="h5">
                { this.props.contact.name }
              </CardTitle>
              <CardText>
                Age: <strong>{ this.props.contact.age }</strong> years old
              </CardText>
              <CardText>
                Phone: <strong>{ this.props.contact.phone }</strong>
              </CardText>
              <CardText>
                LinkedIn:&nbsp;
                <a
                  class="link-primary"
                  target="_blank"
                  href={ this.props.contact.link }>
                  { this.props.contact.link }
                </a>
              </CardText>
            </div>
            <div className="contact-card__bottom">
              <Button
                className="contact-card__button"
                color="primary"
                outline>
                Edit
              </Button>
              <Button
                className="contact-card__button"
                color="danger"
                outline
                onClick={this.props.onRemove.bind(this, this.props.contact.id)}>
                Delete
              </Button>
            </div>
          </Card>
        </div>
      )
    else
      return (
        <div className="contact-card">
          <Card
            body
            className="contact-card__body">
            <CardTitle tag="h5">
              Select a contact
            </CardTitle>
          </Card>
        </div>
      )
  }

}
